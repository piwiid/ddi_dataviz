import BUS from './BUS';

export default class SliderBullet {
    constructor(id, datas) {
        this.id = id;
        this.datas = datas;
    }

    build(id) {
        this.el = document.createElement('li');
        if(id === 0) {
            this.el.className = 'current';
        } else {
            this.el.className = '';
        }

        // Process datas
        //
        this.el.addEventListener('click', () => this.click());
        return this.el;

    }
    click() {
        BUS.dispatch('bulletMove',{detail: {bullet: this.id}});
    }
    hide(dir) {
        this.el.classList.remove('current');
    }
    show(dir) {
        this.el.classList.add('current');
    }
}
