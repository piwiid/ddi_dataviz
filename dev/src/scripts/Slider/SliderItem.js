export default class SliderItem {
    constructor(id, datas) {
        this.id = id;
        this.datas = datas;
        this.tl = new TimelineMax();

    }
    build(id) {
        this.el = document.createElement('article');
        // Process datas
        //
        this.el.innerHTML = `
            <div class="nouveaute__slider nouveaute__slider-item--left">
                <img class="nouveaute__img-slider" src="${this.datas.img}" alt="">
            </div>
            <div class="nouveaute__slider nouveaute__slider-item--right">
                <h3 class="nouveaute__slider-item--tile">${this.datas.title}</h3>
                <p class="nouveaute__slider-item--description">${this.datas.desc}</p>
                <a class="btn-slider btn " href="">commander</a>
            </div>`;
        this.el.innerHTML = `
            <section class="section-home first-block">
                    <div class="wrap-flex">
                        <article class="col-f2 col-f2--right">
                            <h1 class="section_title--left">
                                <span class="section_title--number">${this.datas.id}</span>
                                ${this.datas.title}
                            </h1>
                        </article>
                        <article class="col-f2">
                            <p class="slide-3d">${this.datas.desc}</span></p>
                            <p class="sub-title">${this.datas.subtitle}</p>
                            <a class="btn btn--arrow" href="#">Notre méthodologie</a>
                        </article>
                    </div>
                </section>
            </section>
        `;

        // Animation sur le premier slide quand l'utilisateur arrive
        if(id === 0) {
            this.el.className = 'nouveaute__slider-item current';
            let el = this.el.querySelectorAll('div > *');
            el.forEach((item) => {
                TweenMax.to(item, 1, {
                    autoAlpha:1,
                    x: 0,
                });
            });
        } else {
            this.el.className = 'nouveaute__slider-item';
        }
        return this.el;
    }

    show(dir) {
        let el = this.el.querySelectorAll('div > *');
        TweenMax.delayedCall(2, () => {
            this.el.classList.add('current');
            el.forEach((item) => {
                TweenMax.to(item, 1, {
                    x: 0,
                    autoAlpha:1,
                    delay: `${Math.random() * 1}`,
                });
            });
        });
    }
    hide(dir) {
        let el = this.el.querySelectorAll('div > *');

        // J'utilise un forEach au lieu d'utiliser staggerTo pour pouvoir faire l'effet de décalage sur chaque élément
        el.forEach((item) => {
            TweenMax.to(item, 1, {
                autoAlpha:0,
                delay: `${Math.random() * 1}`,
                x: "-120%",
            });
        });

        TweenMax.delayedCall(2, () => {
            TweenMax.set(el,{clearProps:"all"});
            this.el.classList.remove('current');
        });
    }


}
