import BUS from './BUS';
import SliderItem from './SliderItem';
import SliderBullet from './SliderBullet';
import SliderBullets from './SliderBullets';

// Création de la class Slider
export default class Slider {
    // Déclaration des variables
    // Peut être rajouter url
    constructor(el,dots, arrows) {
        this.el = el;
        this.items = [];
        this.bullets = [];
        this.current = 0;
        this.max = 0;
        this.dots = dots;
        this.arrows = arrows;
    }

    // On passe l'élément à la création
    // Méthode pour initialiser
    init(url) {
        if(this.arrows) {
            this.left = this.el.querySelector('.slide-arrow--left');
            this.right = this.el.querySelector('.slide-arrow--right');
            this.load(url);

        }
        if (this.dots) {
            setTimeout(() => {
                this.right = document.querySelectorAll('.btn-next');
                this.load(url);
            },200)
        }
    }
    // Chargement des données
    load(url) {
        this.listen();
        if(this.dots) {
            var slideItem = this.el.querySelectorAll('.slide--item');
        } else {
            var slideItem = this.el.querySelectorAll('.slide--item-two');
        }
        for(var i = 0; i < slideItem.length; i++) {
            this.items.push(slideItem[i]);
        }
        this.build(slideItem);

    }
    listen () {
        if(this.arrows) {
            this.left.addEventListener('click', () => {
                this.move(-1)
            });
            this.right.addEventListener('click', () => this.move(+1));
        }
        if(this.dots) {
            for (let i = 0; this.right.length > i ;i++) {
                this.right[i].addEventListener('click', () => this.move(+1));
            }
            BUS.listen('bulletMove', (e) => this.bulletMove(e));
        }
    }
    bulletMove(e) {
        this.move(e.detail.bullet - this.current);
    }
    build(datas) {
        this.max = datas.length;
        if(this.dots) {
            var bullets = this.el.querySelector('.nouveaute__dots');
            var i = 0;
            var item;
            for(i; i < this.max; i++){
                var bullet = new SliderBullet(i, datas[i]);
                bullets.appendChild(bullet.build(i));
                this.bullets.push(bullet);
            }
        }
    }
    move(dir) {
        if(this.arrows) {
            let svg = document.getElementById('donuts');
            let svgDay = svg.querySelector('svg');
            let te = svgDay.querySelectorAll('path');
            this.items[this.current];
            this.hide(this.current);
            /*this.bullets[this.current].hide(dir);*/
            this.current = Math.max(-1, Math.min(this.max, this.current + dir));
            if (this.current === this.max) {
                this.current = 0;
            } else if(this.current === -1) {
                this.current = this.max - 1;
            }
            /*this.bullets[this.current].show(dir);*/
            let listeDay = document.querySelectorAll('.list-days-two .list-days-item');
            for(let i = 0; i < listeDay.length; i++) {
                if(listeDay[i].classList.contains('current')) {
                    listeDay[i].classList.remove('current');
                }
            }
            this.items[this.current];
            this.show(this.current);
            let listDayActive = this.items[this.current].getAttribute('data-day');

            for(let i = 0; i < te.length; i++) {
                if(te[i].getAttribute('data-day') === listDayActive) {
                    te[i].style.fill = "url(#svgGradient)";
                } else {
                    te[i].style.fill = "white";

                }
            }
            switch (listDayActive) {
                case "1":
                    TweenMax.to(svgDay,0.4,{
                        rotation: 276 + 'deg'
                    })
                    break;
                case "2":
                    TweenMax.to(svgDay,0.4,{
                        rotation: 185 + 'deg'
                    })
                    break;
                case "3":
                    TweenMax.to(svgDay,0.4,{
                        rotation: 168 + 'deg'
                    })
                    break;
                case "4":
                    TweenMax.to(svgDay,0.4,{
                        rotation: 147 + 'deg'
                    })
                    break;
                case "5":
                    TweenMax.to(svgDay,0.4,{
                        rotation: 127 + 'deg'
                    });
                    break;
                case "6":
                    TweenMax.to(svgDay,0.4,{
                        rotation: 100 + 'deg'
                    })
                    break;
                case "7":
                    TweenMax.to(svgDay,0.4,{
                        rotation: 404 + 'deg'
                    })
                    break;
                case "8":
                    TweenMax.to(svgDay,0.4,{
                        rotation: 5 + 'deg'
                    })
                    break;
                default:
                    console.log("Désolé, nous n'avons plus de " + expr + ".");
            }
        }
        if(this.dots) {
            this.items[this.current];
            console.log(this.current);
            this.hide(this.current);
            this.bullets[this.current].hide(dir);
            this.current = Math.max(-1, Math.min(this.max, this.current + dir));
            if (this.current === this.max) {
                TweenMax.fromTo (('.nouveaute__dots') , 1, {opacity:1}, {opacity:0,display:'none'});
                TweenMax.fromTo (('five-slide') , 1, {opacity:1}, {opacity:0,display:'none'});
                TweenMax.delayedCall(1,() => {
                    TweenMax.fromTo (('.thank') , 1, {opacity:0}, {opacity:1,display:'flex'})
                });

            } else if(this.current === -1) {
                this.current = this.max - 1;
            }
            if(this.current < this.max) {
                console.log("Salut");
                this.bullets[this.current].show(dir);
            }
            this.items[this.current];
            this.show(this.current);
        }


    }
    show(dir) {
        if(this.dots) {
            TweenMax.delayedCall(1,() => {
                TweenMax.fromTo (this.items[dir] , 1, {opacity:0}, {opacity:1,display:'block',
                })
            });
        } else {
            this.items[dir].classList.add('current');
        }
    }
    hide(dir) {
        if(this.dots) {
            TweenMax.fromTo (this.items[dir] , 1, {opacity:1}, {opacity:0,display:'none',});
            this.items[dir].classList.remove('current');
        }
    }
}
