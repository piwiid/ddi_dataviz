export default class allTemplate {
    constructor() {
        let template = `
        <section class="slide first_slide first_slidee slide-template">
		<div class="wrap-flex">
			<div class="col-f2">
				<div class="first_slide-content">
					<div class="wrap-flex first_slide-content-line">
						<span class="first_slide-content-drag"></span>
						<span class="no-select first_slide-content-yes">Oui</span>
						<span class="no-select first_slide-content-no">Non</span>
					</div>
				</div>
			</div>
			<div class="col-f2">
				<div class="results">
					<svg class="d3" width="370" height="370"></svg>
					<p class="results-content">90% des personnes pratiquant l’automédication en france</p>
				</div>
			</div>
		</div>
	</section> 
        `;
        return template;
    }
}
