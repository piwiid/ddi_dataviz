export default {

    dispatch: function(e, datas){
        var event = new CustomEvent(e, datas || {});
        this.el.dispatchEvent(event);
    },

    listen: function(e, method){
        this.el.addEventListener(e, method);
    }
}