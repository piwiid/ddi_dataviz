export default class burger {
    constructor(el) {
        this.el = document.getElementById(el);
        this.listen();
    }
    listen() {
        this.el.addEventListener('click', ()=> {
            this.el.classList.toggle('active');
        })
    }
}