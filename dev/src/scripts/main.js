import Tween from 'gsap';
import axios from 'axios';
/*import Barba from 'barba.js';*/
import BUS from "./BUS";
import Slider from "./Slider/Slider";
import All from './template/all';


let arrayMedic;

// SITE const listMedic = '../www/json/list.json';
const listMedic = '../json/list.json';
let dataSearch;

let responseArray = [];
let tw = TweenMax;
let tlm = TimelineMax;

let slider = new Slider(document.querySelector('.slider-container'), true, false);

slider.init();



let loader = document.querySelector('.pre-loader');
let medicLenght = 6;
let medicSrc = 'images/medic_load.svg';
let listMedicImg = [];

let lastList =  document.querySelector('.template-question--slide-last').querySelectorAll('.list-btn a');
let dataLast;
let lastListResult = document.querySelector('.template-result--slide-last').querySelectorAll('.list-btn a');
let data;

for(let i = 0; lastList.length > i; i++) {
    lastList[i].addEventListener('click', (e) => {
        for(let y = 0; lastList.length > y; y++) {
            if(lastList[y].classList.contains('btn-last')) {
                lastList[y].classList.remove('btn-last');
                lastListResult[y].classList.remove('btn-last');
            }
        }
        e.preventDefault();
        lastList[i].classList.add('btn-last');
        lastListResult[i].classList.add('btn-last');

        dataLast = lastList[i].getAttribute('data-id');
    })
}
let btnLast = document.querySelector('.template-question--slide-last').querySelector('.btn-valid');
let containerCircle = document.querySelector('.circle_medic');
let containerCircleUl = document.querySelector('.list-medic span');
let currentMedic = 0;
let containerLast = document.querySelector('last-result');
let dataSwitch;

let medics = document.querySelectorAll('.pre-loader .medic');
for (let i = 0; medics.length > i; i++) {
    TweenMax.set(medics[i], { transformStyle: "preserve-3d" });
    window.addEventListener('mousemove', (e) =>{
        var sxPos = e.pageX / window.innerWidth * 100 - 100;
        var syPos = e.pageY / window.innerHeight * 100 - 100;
        TweenMax.to(medics[i], 2, {
            rotationY: 0.02 * sxPos,
            rotationX: -0.02 * syPos,
            transformPerspective: 500,
            transformOrigin: "center center -400",
            ease: Expo.easeOut
        });
    });
}




let items = [];
btnLast.addEventListener('click', (e) => {
    e.preventDefault();

    tw.fromTo (('.template-question--slide-last') , 1, {opacity:1}, {opacity:0,display:'none'});
    TweenMax.delayedCall(1,() => {
        tw.fromTo (('.template-result--slide-last') , 1, {opacity:0}, {opacity:1,display:'block'})
    });
// SITE const listMedic = '../www/json/list.json';
    axios.get('../json/list.json')
        .then(function (response) {
            switch (dataLast) {
                case "1":
                    arrayMedic = response.data.rhume;
                    break;
                case "2":
                    arrayMedic = response.data.toux;
                    break;
                case "3":
                    arrayMedic = response.data.gorge;
                    break;
                case "4":
                    arrayMedic = response.data.grippe;
                    break;
                case "5":
                    arrayMedic = response.data.diarrhee;
                    break;
                case "6":
                    arrayMedic = response.data.maux;
                    break;
                case "7":
                    arrayMedic = response.data.nausee;
                    break;
                case "8":
                    arrayMedic = response.data.digestion;
                    break;
                default:
                    console.log("Désolé, nous n'avons plus de " + expr + ".");
            }

            let lenghtMax = arrayMedic.length;
            var centerX = document.querySelector('.circle_medic').getBoundingClientRect().width / 2;
            var centerY = document.querySelector('.circle_medic').getBoundingClientRect().height / 2;
            dataSwitch = arrayMedic[0];

            for (let i = 0; lenghtMax > i; i++) {
                if (i === 0) {
                    containerCircleUl.innerHTML += `<li data-id="${i}" class="current">${arrayMedic[i].name}</li>`;
                    containerCircle.innerHTML += `<div data-id="${i}" class="circle_medic--dots current ${arrayMedic[i].color} "></div>`
                } else {
                    containerCircle.innerHTML += `<div data-id="${i}" class="circle_medic--dots ${arrayMedic[i].color} "></div>`

                }
                /*document.querySelector('.col-f70').innerHTML += response.data.rhume[i].name;*/
            }

            setTimeout(() => {
                var circle = document.querySelector('.circle_medic'),
                    imgs = document.querySelectorAll('.circle_medic--dots'),
                    total = lenghtMax,
                    coords = {},
                    diam, radius1, radius2, imgW;


                // get circle diameter
                // getBoundingClientRect outputs the actual px AFTER transform
                //      using getComputedStyle does the job as we want
                diam = parseInt( window.getComputedStyle(circle).getPropertyValue('width') ),
                    radius = diam/2,
                    imgW = imgs[0].getBoundingClientRect().width,
                    // get the dimensions of the inner circle we want the images to align to
                    radius2 = radius - imgW

                var i,
                    alpha = Math.PI / 2,
                    len = imgs.length,
                    corner = 2 * Math.PI / total;

                for ( i = 0 ; i < total; i++ ){

                    imgs[i].style.left = parseInt( ( radius - imgW / 2 ) + ( radius2 * Math.cos( alpha ) ) ) + 'px'
                    imgs[i].style.top =  parseInt( ( radius - imgW / 2 ) - ( radius2 * Math.sin( alpha ) ) ) + 'px'

                    alpha = alpha - corner;
                }
                let allCircle = document.querySelectorAll('.circle_medic--dots');
                for (let i = 0; allCircle.length > i; i++) {
                    allCircle[i].addEventListener('click',(e) => {
                        for(let y = 0; allCircle.length > y; y++) {
                            if(allCircle[y].classList.contains('current')) {
                                allCircle[y].classList.remove('current');
                            }
                        }
                        document.querySelector(".list-medic span").innerHTML = `<li data-id="${i}">${response.arrayMedic[i].name}</li>`;
                        allCircle[i].classList.add('current');
                        dataSwitch = arrayMedic[i];
                        changeMedic(dataSwitch);
                        resetButtonListe();
                        currentMedic = i;
                    })
                }

            let prevMedic = document.getElementById('next-medic');
            let nextMedic = document.getElementById('prev-medic');
            nextMedic.addEventListener('click', (e) => {
                currentMedic += 1;
                if(currentMedic === lenghtMax) {
                    currentMedic = 0;
                }
                for(let y = 0; allCircle.length > y; y++) {
                    if(allCircle[y].classList.contains('current')) {
                        allCircle[y].classList.remove('current');
                    }
                }

                allCircle[currentMedic].classList.add('current');
                dataSwitch = arrayMedic[currentMedic];
                changeMedic(dataSwitch);
                resetButtonListe();
                document.querySelector(".list-medic span").innerHTML = `<li data-id="${currentMedic}">${arrayMedic[currentMedic].name}</li>`;
            });
            prevMedic.addEventListener('click', (e) => {
                if(currentMedic ===  0) {
                    currentMedic = lenghtMax - 1;
                } else {
                    currentMedic -= 1;
                }
                for(let y = 0; allCircle.length > y; y++) {
                    if(allCircle[y].classList.contains('current')) {
                        allCircle[y].classList.remove('current');
                    }
                }
                dataSwitch = arrayMedic[currentMedic];
                changeMedic(dataSwitch);
                resetButtonListe();
                allCircle[currentMedic].classList.add('current');
                document.querySelector(".list-medic span").innerHTML = `<li data-id="${currentMedic}">${arrayMedic[currentMedic].name}</li>`;

            });
            }, 1100)

            document.querySelector('.last-result').innerHTML = `
        <div class="wrap-flex last-result">
            <div class="name_medic-content">
                <div class="content_name">
                     <span class="round_medic ${arrayMedic[0].color}"></span>
                    <h2 class="name_medic">${arrayMedic[0].name}</h2>
                </div>
                <p class="name_price">${arrayMedic[0].price}</p>

            </div>
            <div class="instruction">
                <h2>Insctructions</h2>
                <div class="wrap-flex">
                    <div class="col-f2">
                        <h3>Contre-indications</h3>
                        <p>
                            ${arrayMedic[0].contre}
                        </p>
                    </div>
                    <div class="col-f2">
                        <h3>Ne pas mélanger avec</h3>
                        <p${arrayMedic[0].jamaais}</p>
                    </div>
                </div>
            </div>
        </div>
        `;



            changeMedic(dataSwitch);

        })
    dataTest();
})

// FUNCTION CHANGE MEDIC
function changeMedic(data) {

    document.querySelector('.last-result').innerHTML = `
        <div class="wrap-flex last-result">
            <div class="name_medic-content">
                <div class="content_name">
                     <span class="round_medic ${data.color}"></span>
                    <h2 class="name_medic">${data.name}</h2>
                </div>
                <p class="name_price">${data.price}</p>

            </div>
            <div class="instruction">
                <h2>Insctructions</h2>
                <div class="wrap-flex">
                    <div class="col-f2">
                        <h3>Contre-indications</h3>
                        <p>
                            ${data.contre}
                        </p>
                    </div>
                    <div class="col-f2">
                        <h3>Ne pas mélanger avec</h3>
                        <p${data.jamaais}</p>
                    </div>
                </div>
            </div>
        </div>
        `;


    swithLast(data)

}

// PARTIE SWITCH BOUTON LAST
function swithLast(data) {
    document.querySelector('.btn-instru').addEventListener('click', function(e) {
        if(document.querySelector('.btn-retenir').classList.contains('active')) {
            document.querySelector('.btn-retenir').classList.remove('active');
            document.querySelector('.btn-instru').classList.add('active');
        }
        e.preventDefault();
        document.querySelector('.last-result').innerHTML = `
        <div class="wrap-flex last-result">
            <div class="name_medic-content">
                <div class="content_name">
                     <span class="round_medic ${data.color}"></span>
                    <h2 class="name_medic">${data.name}</h2>
                </div>
                <p class="name_price">${data.price}</p>

            </div>
            <div class="instruction">
                <h2>Insctructions</h2>
                <div class="wrap-flex">
                    <div class="col-f2">
                        <h3>Contre-indications</h3>
                        <p>
                            ${data.contre}
                        </p>
                    </div>
                    <div class="col-f2">
                        <h3>Ne pas mélanger avec</h3>
                        <p${data.jamaais}</p>
                    </div>
                </div>
            </div>
        </div>
        `;
    });
    document.querySelector('.btn-retenir').addEventListener('click', (e) => {
        e.preventDefault();
        if(document.querySelector('.btn-instru').classList.contains('active')) {
            document.querySelector('.btn-instru').classList.remove('active');
            document.querySelector('.btn-retenir').classList.add('active');
        }
        document.querySelector('.instruction').innerHTML = `
            <div class="instruction retenir">
                <h2>À retenir</h2>
                <p class="content-retenir">
                La publicité permet une "efficacité" pour ne pas "être arrêté par un vilain rhume". Séduisant, mais dangereux. 3 comprimés blancs avec un vasoconstricteur à prendre matin, midi et soir, et un comprimé bleu avec un antihistaminique à prendre au coucher. Ces substances ne sont pas justifiées en raison de risques trop nombreux (accidents cardiovasculaires, neurologiques, psychiatriques...).
                </p>
        </div>

        `;
    }, false);
}

function resetButtonListe() {
    document.querySelector('.btn-retenir').classList.remove('active');
    document.querySelector('.btn-instru').classList.add('active');
}
// RECONSTRUIRE

let dataLastResult;
let lastListResultFinal = document.querySelector('.last-liste').querySelectorAll('.list-btn a');
let listeBuild;
for(let i = 0; lastListResultFinal.length > i; i++) {
    lastListResultFinal[i].addEventListener('click', (e) => {
        for(let y = 0; lastListResultFinal.length > y; y++) {
            if(lastListResultFinal[y].classList.contains('btn-last')) {
                lastListResultFinal[y].classList.remove('btn-last');
                lastListResult[y].classList.remove('btn-last');
            }
        }
        e.preventDefault();
        lastListResultFinal[i].classList.add('btn-last');
        lastListResultFinal[i].classList.add('btn-last');

        listeBuild = lastListResultFinal[i].getAttribute('data-id');
        axios.get('../json/list.json')
            .then(function (response) {
                switch (listeBuild) {
                    case "1":
                        arrayMedic = response.data.rhume;
                        break;
                    case "2":
                        arrayMedic = response.data.toux;
                        break;
                    case "3":
                        arrayMedic = response.data.gorge;
                        break;
                    case "4":
                        arrayMedic = response.data.grippe;
                        break;
                    case "5":
                        arrayMedic = response.data.diarrhee;
                        break;
                    case "6":
                        arrayMedic = response.data.maux;
                        break;
                    case "7":
                        arrayMedic = response.data.nausee;
                        break;
                    case "8":
                        arrayMedic = response.data.digestion;
                        break;
                    default:
                        console.log("Désolé, nous n'avons plus de " + expr + ".");
                }
                currentMedic = 0;
                document.querySelector('.circle_medic').innerHTML = `
                <ul class="list-medic">
                    <div id="prev-medic"></div>
                    <div id="next-medic"></div>
                    <span></span>
                </ul>
                `;
                resetButtonListe();

                let lenghtMax = arrayMedic.length;
                var centerX = document.querySelector('.circle_medic').getBoundingClientRect().width / 2;
                var centerY = document.querySelector('.circle_medic').getBoundingClientRect().height / 2;
                dataSwitch = arrayMedic[0];

                for (let i = 0; lenghtMax > i; i++) {
                    if (i === 0) {
                        document.querySelector('.template-result .circle_medic span').innerHTML += `<li data-id="${i}" class="current">${arrayMedic[i].name}</li>`;
                        containerCircle.innerHTML += `<div data-id="${i}" class="circle_medic--dots current ${arrayMedic[i].color} "></div>`
                    } else {
                        containerCircle.innerHTML += `<div data-id="${i}" class="circle_medic--dots ${arrayMedic[i].color} "></div>`

                    }
                    /*document.querySelector('.col-f70').innerHTML += response.data.rhume[i].name;*/
                }

                var circle = document.querySelector('.circle_medic'),
                    imgs = document.querySelectorAll('.circle_medic--dots'),
                    total = lenghtMax,
                    coords = {},
                    diam, radius1, radius2, imgW;


                // get circle diameter
                // getBoundingClientRect outputs the actual px AFTER transform
                //      using getComputedStyle does the job as we want
                diam = parseInt( window.getComputedStyle(circle).getPropertyValue('width') ),
                    radius = diam/2,
                    imgW = imgs[0].getBoundingClientRect().width,
                    // get the dimensions of the inner circle we want the images to align to
                    radius2 = radius - imgW

                var i,
                    alpha = Math.PI / 2,
                    len = imgs.length,
                    corner = 2 * Math.PI / total;

                for ( i = 0 ; i < total; i++ ){

                    imgs[i].style.left = parseInt( ( radius - imgW / 2 ) + ( radius2 * Math.cos( alpha ) ) ) + 'px'
                    imgs[i].style.top =  parseInt( ( radius - imgW / 2 ) - ( radius2 * Math.sin( alpha ) ) ) + 'px'

                    alpha = alpha - corner;
                }
                let allCircle = document.querySelectorAll('.circle_medic--dots');
                for (let i = 0; allCircle.length > i; i++) {
                    allCircle[i].addEventListener('click',(e) => {
                        for(let y = 0; allCircle.length > y; y++) {
                            if(allCircle[y].classList.contains('current')) {
                                allCircle[y].classList.remove('current');
                            }
                        }
                        document.querySelector(".list-medic span").innerHTML = `<li data-id="${i}">${response.data.rhume[i].name}</li>`;
                        allCircle[i].classList.add('current');
                        dataSwitch = arrayMedic[i];
                        changeMedic(dataSwitch);
                        resetButtonListe();
                        currentMedic = i;
                    })
                }
                let prevMedic = document.getElementById('next-medic');
                let nextMedic = document.getElementById('prev-medic');

                nextMedic.addEventListener('click', (e) => {
                    currentMedic += 1;
                    if(currentMedic === lenghtMax) {
                        currentMedic = 0;
                    }
                    for(let y = 0; allCircle.length > y; y++) {
                        if(allCircle[y].classList.contains('current')) {
                            allCircle[y].classList.remove('current');
                        }
                    }

                    allCircle[currentMedic].classList.add('current');
                    dataSwitch = arrayMedic[currentMedic];
                    changeMedic(dataSwitch);
                    resetButtonListe();
                    document.querySelector(".list-medic span").innerHTML = `<li data-id="${currentMedic}">${arrayMedic[currentMedic].name}</li>`;
                });
                prevMedic.addEventListener('click', (e) => {
                    if(currentMedic ===  0) {
                        currentMedic = lenghtMax - 1;
                    } else {
                        currentMedic -= 1;
                    }
                    for(let y = 0; allCircle.length > y; y++) {
                        if(allCircle[y].classList.contains('current')) {
                            allCircle[y].classList.remove('current');
                        }
                    }
                    dataSwitch = arrayMedic[currentMedic];
                    changeMedic(dataSwitch);
                    resetButtonListe();

                    allCircle[currentMedic].classList.add('current');
                    document.querySelector(".list-medic span").innerHTML = `<li data-id="${currentMedic}">${arrayMedic[currentMedic].name}</li>`;

                });


                document.querySelector('.last-result').innerHTML = `
        <div class="wrap-flex last-result">
            <div class="name_medic-content">
                <div class="content_name">
                     <span class="round_medic ${arrayMedic[0].color}"></span>
                    <h2 class="name_medic">${arrayMedic[0].name}</h2>
                </div>
                <p class="name_price">${arrayMedic[0].price}</p>

            </div>
            <div class="instruction">
                <h2>Insctructions</h2>
                <div class="wrap-flex">
                    <div class="col-f2">
                        <h3>Contre-indications</h3>
                        <p>
                            ${arrayMedic[0].contre}
                        </p>
                    </div>
                    <div class="col-f2">
                        <h3>Ne pas mélanger avec</h3>
                        <p${arrayMedic[0].jamaais}</p>
                    </div>
                </div>
            </div>
        </div>
        `;

                changeMedic(dataSwitch);

            })
    })
}


function dataTest() {
    for(let i = 0; lastListResult.length > i; i++) {
        lastListResult[i].addEventListener('click', (e) => {
            for(let y = 0; lastListResult.length > y; y++) {
                if(lastListResult[y].classList.contains('btn-last')) {
                    lastListResult[y].classList.remove('btn-last');
                }
            }
            e.preventDefault();
            lastListResult[i].classList.add('btn-last');
            dataLast = lastListResult[i].getAttribute('data-sick');
        })
    }
}


const listMedicament = '../json/list_search.json';
let medicaments = [];


fetch(listMedicament)
    .then(blob => blob.json())
    .then(data => medicaments.push(...data.medicaments));

function trouverMedicament(recherche, medicaments) {
    return medicaments.filter(medicament => {
        const regex = new RegExp(recherche, 'gi');
        return medicament.name.match(regex)
    });

}
function afficherResutl() {
    const arrayMedic = trouverMedicament(this.value, medicaments);
    document.querySelector('.circle_medic').innerHTML = `
                <ul class="list-medic">
                    <div id="prev-medic"></div>
                    <div id="next-medic"></div>
                    <span></span>
                </ul>
                `;
    resetButtonListe();

    currentMedic = 0;

    let lenghtMax = arrayMedic.length;
    var centerX = document.querySelector('.circle_medic').getBoundingClientRect().width / 2;
    var centerY = document.querySelector('.circle_medic').getBoundingClientRect().height / 2;
    dataSwitch = arrayMedic[0];

    for (let i = 0; lenghtMax > i; i++) {
        if (i === 0) {
            document.querySelector('.template-result .circle_medic span').innerHTML += `<li data-id="${i}" class="current">${arrayMedic[i].name}</li>`;
            containerCircle.innerHTML += `<div data-id="${i}" class="circle_medic--dots current ${arrayMedic[i].color} "></div>`
        } else {
            containerCircle.innerHTML += `<div data-id="${i}" class="circle_medic--dots ${arrayMedic[i].color} "></div>`

        }
        /*document.querySelector('.col-f70').innerHTML += response.data.rhume[i].name;*/
    }

    var circle = document.querySelector('.circle_medic'),
        imgs = document.querySelectorAll('.circle_medic--dots'),
        total = lenghtMax,
        coords = {},
        diam, radius1, radius2, imgW;


    // get circle diameter
    // getBoundingClientRect outputs the actual px AFTER transform
    //      using getComputedStyle does the job as we want
    diam = parseInt( window.getComputedStyle(circle).getPropertyValue('width') ),
        radius = diam/2,
        imgW = imgs[0].getBoundingClientRect().width,
        // get the dimensions of the inner circle we want the images to align to
        radius2 = radius - imgW

    var i,
        alpha = Math.PI / 2,
        len = imgs.length,
        corner = 2 * Math.PI / total;

    for ( i = 0 ; i < total; i++ ){

        imgs[i].style.left = parseInt( ( radius - imgW / 2 ) + ( radius2 * Math.cos( alpha ) ) ) + 'px'
        imgs[i].style.top =  parseInt( ( radius - imgW / 2 ) - ( radius2 * Math.sin( alpha ) ) ) + 'px'

        alpha = alpha - corner;
    }
    let allCircle = document.querySelectorAll('.circle_medic--dots');
    for (let i = 0; allCircle.length > i; i++) {
        allCircle[i].addEventListener('click',(e) => {
            for(let y = 0; allCircle.length > y; y++) {
                if(allCircle[y].classList.contains('current')) {
                    allCircle[y].classList.remove('current');
                }
            }
            document.querySelector(".list-medic span").innerHTML = `<li data-id="${i}">${arrayMedic[i].name}</li>`;
            allCircle[i].classList.add('current');
            dataSwitch = arrayMedic[i];
            changeMedic(dataSwitch);
            resetButtonListe();
            currentMedic = i;
        })
    }
    let prevMedic = document.getElementById('next-medic');
    let nextMedic = document.getElementById('prev-medic');

    nextMedic.addEventListener('click', (e) => {
        currentMedic += 1;
        if(currentMedic === lenghtMax) {
            currentMedic = 0;
        }
        for(let y = 0; allCircle.length > y; y++) {
            if(allCircle[y].classList.contains('current')) {
                allCircle[y].classList.remove('current');
            }
        }

        allCircle[currentMedic].classList.add('current');
        dataSwitch = arrayMedic[currentMedic];
        changeMedic(dataSwitch);
        resetButtonListe();
        document.querySelector(".list-medic span").innerHTML = `<li data-id="${currentMedic}">${arrayMedic[currentMedic].name}</li>`;
    });
    prevMedic.addEventListener('click', (e) => {
        if(currentMedic ===  0) {
            currentMedic = lenghtMax - 1;
        } else {
            currentMedic -= 1;
        }
        for(let y = 0; allCircle.length > y; y++) {
            if(allCircle[y].classList.contains('current')) {
                allCircle[y].classList.remove('current');
            }
        }
        dataSwitch = arrayMedic[currentMedic];
        changeMedic(dataSwitch);
        resetButtonListe();

        allCircle[currentMedic].classList.add('current');
        document.querySelector(".list-medic span").innerHTML = `<li data-id="${currentMedic}">${arrayMedic[currentMedic].name}</li>`;

    });


    document.querySelector('.last-result').innerHTML = `
        <div class="wrap-flex last-result">
            <div class="name_medic-content">
                <div class="content_name">
                     <span class="round_medic ${arrayMedic[0].color}"></span>
                    <h2 class="name_medic">${arrayMedic[0].name}</h2>
                </div>
                <p class="name_price">${arrayMedic[0].price}</p>

            </div>
            <div class="instruction">
                <h2>Insctructions</h2>
                <div class="wrap-flex">
                    <div class="col-f2">
                        <h3>Contre-indications</h3>
                        <p>
                            ${arrayMedic[0].contre}
                        </p>
                    </div>
                    <div class="col-f2">
                        <h3>Ne pas mélanger avec</h3>
                        <p${arrayMedic[0].jamaais}</p>
                    </div>
                </div>
            </div>
        </div>
        `;

    changeMedic(dataSwitch);


}

const input = document.querySelector('input');
const resultat = document.querySelector('.list-medic');

input.addEventListener('change', afficherResutl);
input.addEventListener('keyup', afficherResutl);

// wave
const canvas = document.querySelector('canvas');
const ctx = document.querySelector('canvas').getContext('2d');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// Set waves opacities
const wavesOpacities = [0.07, 0.1, 0.09, 0.08, 0.07, 0.06, 0.05, 0.04];

// Set parameters
const params = {
    AMPLITUDE_WAVES: 913,
    AMPLITUDE_MIDDLE: 304,
    AMPLITUDE_SIDES: 457,
    OFFSET_SPEED: 120,
    SPEED: 0.4,
    OFFSET_WAVES: 10,
    NUMBER_WAVES: 2,
    COLOR: '#fafafa',
    NUMBER_CURVES: 4,
    OFFSET_CURVE: true,
    RESET: false
};
let speedInc = 0;

// Set gradient colors

// Convert Hex to RGB for gradient
const hexToRgb = (hex) => {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
};

let rgb = hexToRgb(params.COLOR);
let gradient = ctx.createLinearGradient(0, 0, 0, canvas.height);
gradient.addColorStop(0, `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0)`);
gradient.addColorStop(1, `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 1)`);

// Render

const render = () => {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    // For each wave
    for (let j = params.NUMBER_WAVES - 1; j >= 0; j--) {
        // offset between waves
        let offset = speedInc + j * Math.PI * params.OFFSET_WAVES;

        // Color and increase gradually opacity
        if (j === 0) {
            ctx.fillStyle = gradient;
        } else {
            ctx.fillStyle = params.COLOR;
        }
        ctx.globalAlpha = wavesOpacities[j];
        let leftRange = ((Math.sin((offset / params.OFFSET_SPEED) + 2) + 1) / 2 * params.AMPLITUDE_SIDES) + (canvas.height - params.AMPLITUDE_SIDES) / 2;
        let rightRange = ((Math.sin((offset / params.OFFSET_SPEED) + 2) + 1) / 2 * params.AMPLITUDE_SIDES) + (canvas.height - params.AMPLITUDE_SIDES) / 2;

        let leftCurveRange = (Math.sin((offset / params.OFFSET_SPEED) + 2) + 1) / 2 * params.AMPLITUDE_WAVES + (canvas.height - params.AMPLITUDE_WAVES) / 2;
        let rightCurveRange = (Math.sin((offset / params.OFFSET_SPEED) + 1) + 1) / 2 * params.AMPLITUDE_WAVES + (canvas.height - params.AMPLITUDE_WAVES) / 2;

        let endCurveRange = ((Math.sin((offset / params.OFFSET_SPEED) + 2) + 1) / 2 * params.AMPLITUDE_MIDDLE) + (canvas.height - params.AMPLITUDE_MIDDLE) / 2;

        let reverseLeftCurveRange = endCurveRange - rightCurveRange + endCurveRange;
        let reverseRightCurveRange = endCurveRange - leftCurveRange + endCurveRange;

        if (params.OFFSET_CURVE === false) {

            leftCurveRange = rightCurveRange;
            reverseRightCurveRange = reverseLeftCurveRange;

        }


        ctx.beginPath();

        ctx.moveTo(0, leftRange);

        ctx.bezierCurveTo(canvas.width / (params.NUMBER_CURVES * 3), leftCurveRange, canvas.width / (params.NUMBER_CURVES * 3 / 2), rightCurveRange, canvas.width / params.NUMBER_CURVES, endCurveRange);

        for (let i = 1; i < params.NUMBER_CURVES; i++) {

            const finalRightCurveRange = i % 2 !== 0 ? rightCurveRange : reverseRightCurveRange;
            const finalLeftCurveRange = i % 2 !== 0 ? leftCurveRange : reverseLeftCurveRange;

            const secondPtX = canvas.width * (i / params.NUMBER_CURVES) + canvas.width / (params.NUMBER_CURVES * 3);
            const secondPtY = endCurveRange - finalRightCurveRange + endCurveRange;
            const thirdPtX = canvas.width * (i / params.NUMBER_CURVES) + canvas.width * (2 / (params.NUMBER_CURVES * 3));
            const thirdPtY = endCurveRange - finalLeftCurveRange + endCurveRange;
            const lastPtX = canvas.width * ((i + 1) / params.NUMBER_CURVES);
            const lastPtY = i === params.NUMBER_CURVES - 1 ? rightRange : endCurveRange;

            ctx.bezierCurveTo(secondPtX, secondPtY, thirdPtX, thirdPtY, lastPtX, lastPtY);

        }

        // Draw last lines

        ctx.lineTo(canvas.width, canvas.height);
        ctx.lineTo(0, canvas.height);
        ctx.lineTo(0, rightRange);

        ctx.closePath();
        ctx.fill();
    }

    // Speed
    speedInc += params.SPEED;
};
// RAF
TweenMax.ticker.addEventListener('tick', render);

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}
function deleteElement(elementDelete) {
    let element = document.querySelector(elementDelete);
    return element.outerHTML = "";
}
function textAnimation(text,select) {
    var selection = document.querySelector(select);

    for (let i = 0; i < text.length; i++) {
        selection.innerHTML += `<div>${text[i]}</div>`;

    }
}

/*textAnimation('Automédication','.quote');*/
/*let canvas = document.getElementById('myCanvas'),
    c = canvas.getContext('2d'),
    w = window.innerWidth,
    h = window.innerHeight;
    canvas.width = document.body.clientWidth;
    canvas.height = document.body.clientHeight;*/


// A reset quand on change de slide


var test = document.querySelectorAll('.btn-round');
for(var i = 0; i < test.length; i++) {
    test[i].addEventListener('click', function() {
        this.classList.toggle('active');
    });
}

window.addEventListener('load',function(){


    let loader = document.querySelector('.loader-js');
    let time = Math.random() * 3
    tw.to(loader, time, {
        scaleX: 1,
        force3D: true,
    })
    TweenMax.delayedCall(time,() => {
        tw.fromTo (('.load') , 1, {opacity:1}, {opacity:0,display:'none'});
        tw.fromTo (('.loader-text') , 1, {opacity:1}, {opacity:0,display:'none'});

        TweenMax.delayedCall(1,() => {
            tw.fromTo (('.introduction') , 1, {opacity:0}, {opacity:1,display:'block'})
        });
    });



    var tl = new TimelineLite;
    let percentageOne;

    let proscrire = 0;
    let favorables = 0;


    // PAGE 1
    var start = document.querySelector('.btn');
    start.addEventListener('click', function(e) {
        e.preventDefault();
        tl.reverse();
        tw.fromTo (('.introduction') , 1, {opacity:1}, {
            opacity:0,
            display:'none',
            onComplete: () => {
                tw.fromTo (('.pre-loader') , 1, {opacity:1}, {opacity:0,display:'none'});
                TweenMax.delayedCall(1,() => {
                    tw.fromTo (('.slider-container') , 1, {opacity:0}, {opacity:1,display:'block'})
                });

                TweenMax.delayedCall(1,() => {
                    deleteElement('.pre-loader');
                    tw.fromTo (('.first_slide') , 1, {opacity:0}, {opacity:1,display:'block'})
                });
                deleteElement('.introduction');
                let resultDag;
                let resultOne;
                let valueDrag;
                let draggableFirst = Draggable.create(".first_slide-content-drag", {
                    type:"x",
                    bounds:".first_slide-content-line",
                    throwProps:true,
                    onDrag: function() {
                        percentageOne = document.querySelector('.first_slide-content-line').getBoundingClientRect().width;
                        let x = document.querySelector('.first_slide-content-drag').getBoundingClientRect().x;
                        let left = document.querySelector('.first_slide-content-line').getBoundingClientRect().left;
                        let tmp = x - left;
                        resultDag = (tmp / 590 * 100);
                        valueDrag = this.x;
                    }
                });
                let test2 = document.querySelector('.btn-valid');
                test2.addEventListener('click', function (e) {
                    if(valueDrag > 0) {
                        resultOne = 'non';
                        firstSlide(resultOne, resultDag);
                    } else if(valueDrag < 0) {
                        resultOne = 'oui';
                        firstSlide(resultOne, resultDag);
                    } else if(valueDrag === 0 || valueDrag === null || valueDrag === undefined) {
                        alert('Veuillez choisir une réponse');
                    }
                });
            },
        });


    });
    // FIN PAGE 1


    // SLIDE 5
    let questionFive = document.querySelectorAll('.five-slide .template-question .btn-choice');
    let resultFive = document.querySelectorAll('.five-slide .template-result .btn-choice');

    for(let i = 0; questionFive.length > i; i++) {
        questionFive[i].addEventListener('click', () => {
            questionFive[i].classList.toggle('active-btn');
            if(questionFive[i].getAttribute('data-medic') === resultFive[i].getAttribute('data-medic')) {
                let dataMedic = resultFive[i].getAttribute('data-medic');
                switch (dataMedic) {
                    case "1":
                        resultFive[0].classList.add('true-btn');
                        favorables += 1;
                        break;
                    case "2":
                        resultFive[1].classList.add('false-btn');
                        proscrire += 1;
                        break;
                    case "3":
                        resultFive[2].classList.add('false-btn');
                        proscrire += 1;
                        break;
                    case "4":
                        resultFive[3].classList.add('false-btn');
                        proscrire += 1;
                        break;
                    case "5":
                        resultFive[4].classList.add('false-btn');
                        proscrire += 1;
                        break;
                    case "6":
                        break;
                    case "7":
                        resultFive[6].classList.add('true-btn');
                        favorables += 1;

                        break;
                    case "8":
                        resultFive[7].classList.add('false-btn');
                        proscrire += 1;
                        break;
                    case "9":
                        break;
                    case "10":
                        resultFive[9].classList.add('false-btn');
                        proscrire += 1;

                        break;
                    default:
                        console.log("Désolé, nous n'avons plus de " + expr + ".");
                }

            }
        });
    }

    let btnFive = document.querySelector('.five-slide').querySelector('.btn-valid');
    btnFive.addEventListener('click', () => {
        tw.fromTo (('.five-slide .template-question') , 1, {opacity:1}, {opacity:0,display:'none'});
        TweenMax.delayedCall(1,() => {
            tw.fromTo ('.five-slide .template-result' , 1, {opacity:0}, {opacity:1,display:'block'})
        });

        let text = document.querySelector('.template-question--slide-five .four_content');
        if(proscrire > 0 && favorables === 0) {
            text.innerHTML = `Sur votre sélection de médicaments : ${proscrire} sont à proscrire `;
        } else if(favorables > 0 && proscrire === 0) {
            text.innerHTML = `Sur votre sélection de médicaments : ${favorables} sont favorables `;
        } else if(favorables > 0 && proscrire > 0) {
            text.innerHTML = `Sur votre sélection de médicaments : ${proscrire} sont à proscrire et ${favorables} sont favorables. `;
        } else if(favorables === 0 && proscrire === 0) {
            text.innerHTML = `Aucun`;
        }

    });
    // FIN PAGE 5
});



function firstSlide(result, resultDag) {
    let valueCss;
    if(resultDag  < 50) {
        valueCss = `calc(${resultDag}% + 25px)`
    } else {
        valueCss = `calc(${resultDag}% - 25px)`
    }
    let slideOne = document.querySelector('.template-result--slide-first');
    tw.set('.first_slide-content-drag--two', {
        position: 'absolute',
        left: valueCss,
    })
    tw.fromTo (document.querySelector('.btn-valid').parentNode , 1, {opacity:1}, {opacity:0,display:'none'});
    TweenMax.delayedCall(1,() => {
        tw.fromTo (slideOne , 1, {opacity:0}, {opacity:1,display:'block'})
        tw.fromTo ('.first-slide-template' , 1, {opacity:0}, {opacity:1,display:'block'})

    });

    if(result === 'oui') {
        var data = [
            {name: "Oui",    value:  80, dacalage: 0, data: 1},
            {name: "Non",    value:  20, dacalage: 30},
            {name: "Display",    value:  100, dacalage: 0},
        ];
    } else {
        var data = [
            {name: "Oui",    value:  80, dacalage: 0},
            {name: "Non",    value:  20, dacalage: 30, data: 1},
            {name: "Display",    value:  100, dacalage: 0},
        ];
    }

    var margin = {top: 20, right: 20, bottom: 30, left: 40},
        width = 350 - margin.left - margin.right,
        height = 340 - margin.top - margin.bottom;

// set the ranges
    var x = d3.scaleBand()
        .range([0, width])
        .padding(0.1);
    var decalage = d3.scaleBand()
        .range([0, width])
        .padding(0.1);
    var y = d3.scaleLinear()
        .range([height, 0]);

// append the svg object to the body of the page
// append a 'group' element to 'svg'
// moves the 'group' element to the top left margin
    var svgOne = d3.select(".chart").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")

        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    // Scale the range of the data in the domains
    x.domain(data.map(function(d) { return d.name; }));

    y.domain([0, d3.max(data, function(d) { return d.value; })]);

    // append the rectangles for the bar chart
    svgOne.selectAll(".bar")
        .data(data)
        .enter().append("rect")
        .attr("class", "bar")
        .attr('rx', 10)
        .attr('ry', 10)
        .attr("x", function(d) { return x(d.name); })
        .attr("width", x.bandwidth() + 30)
        .attr("y", function(d) { return y(d.value); })
        .attr("transform", function( d, i ) { return "translate(" + (i * 40) + "," + 0 + ")" } )
        .attr("height", function(d) { return height - y(d.value); })
        .attr("class", function(d) { if(d.data) {return "bar active-" + d.data} else {
            return "bar";
        } });
    // add the y Axis
    svgOne.append("g")
        .call(d3.axisLeft(y));

    // chart title
    svgOne.append("text")
        .text("(En %)")
        .attr("x", 10)
        .attr("y", 0)
        .attr("class","title");

    var gradientOne = svgOne.append("linearGradient")
        .attr("id", "svgGradientOne")
        .attr("x1", "347.97")
        .attr("x2", "25.99")
        .attr("y1", "141.94")
        .attr("y2", "463.92")
        .attr('gradientUnits','userSpaceOnUse');

    gradientOne.append("stop")
        .attr('class', 'start')
        .attr("offset", "0%")
        .attr("stop-color", "#e5ea23")
        .attr("stop-opacity", 1);

    gradientOne.append("stop")
        .attr('class', 'end')
        .attr("offset", "1")
        .attr("stop-color", "#e08a36")
        .attr("stop-opacity", 1);

    var svgg = document.querySelector('.chart');
    setTimeout(() => {
        let map = document.querySelector('.active-1').getBoundingClientRect();
        console.log(map.top, map.left);
        let p = document.createElement("p");
        p.className = 'map';
        document.querySelector('.first-slide-template').appendChild(p);

        tw.set(p, {
            top: map.top - 40,
            left: map.left + 32,
        });
        window.addEventListener('resize', () => {
            let map = document.querySelector('.active-1').getBoundingClientRect();

            console.log(map.top);
            tw.set(p, {
                top: map.top - 40,
                left: map.left + 32,
            });
        });
    },1200)
}

const list = document.querySelector('.list-days');
let isDown = false;
let startY;
let scrollTop;
let timer;
let liste = document.querySelectorAll('.list-days li');
let selectlist = document.querySelector('.select-list');
let listItemActive;
let scrollDay;
let lastItem;
let scrolLItem;

list.addEventListener('scroll', () => {
    let testo = selectlist.getBoundingClientRect();

    isDown = false;
    let listItem;
    for(let i = 0; i < liste.length; i++) {
        listItem = liste[i].getBoundingClientRect();
        if (listItem.x < testo.x + testo.width &&
            listItem.x + listItem.width > testo.x &&
            listItem.y < testo.y + testo.height/2 &&
            listItem.height/2 + listItem.y > testo.y) {
            liste[i].classList.add('active-liste');
            listItemActive = liste[i];
            lastItem = liste[i];
        } else {
            liste[i].classList.remove('active-liste');
        }
    }
    clearTimeout(timer);
    timer = setTimeout( refresh , 1000 );
    function refresh() {
        let paralistItemActive = listItemActive.getBoundingClientRect();
        scrollDay = listItemActive.getAttribute('data-day');
    }
});
let secondSlide = document.querySelector('.second_slide-content');
let btnSecond = secondSlide.querySelector('.btn-valid');

btnSecond.addEventListener('click', () => {

    setTimeout(() => {
        let slideDay = document.querySelectorAll('.slider-days li');
        let allSelectDay = document.querySelectorAll('.list-days-two--left li');
        let tes;
        for(let i = 0; i < allSelectDay.length; i++) {
            if(allSelectDay[i].getAttribute('data-day') === scrollDay) {
                if(allSelectDay[i].getAttribute('data-day') === '8') {
                    tes = (allSelectDay[i].getBoundingClientRect().top - document.querySelector('.list-days-two').getBoundingClientRect().top);
                } else {
                    tes = (allSelectDay[i].getBoundingClientRect().top - document.querySelector('.list-days-two').getBoundingClientRect().top - 40);
                }
                allSelectDay[i].classList.add('active-liste');
                slideDay[i].classList.add('current');
                allSelectDay[i].classList.add('current');
                document.querySelector('.list-days-two--left').scrollTo(0,tes);

                let listDayActive = slideDay[i].getAttribute('data-day');
                let svgDay = document.querySelector('#donuts svg');
                let te = svgDay.querySelectorAll('path');

                for(let i = 0; i < te.length; i++) {
                    if(te[i].getAttribute('data-day') === listDayActive) {
                        te[i].style.fill = "url(#svgGradient)";
                    } else {
                        te[i].style.fill = "white";

                    }
                }

                switch (listDayActive) {
                    case "1":
                        TweenMax.to(svgDay,0.4,{
                            rotation: 276 + 'deg'
                        })
                        break;
                    case "2":
                        TweenMax.to(svgDay,0.4,{
                            rotation: 185 + 'deg'
                        })
                        break;
                    case "3":
                        TweenMax.to(svgDay,0.4,{
                            rotation: 168 + 'deg'
                        })
                        break;
                    case "4":
                        TweenMax.to(svgDay,0.4,{
                            rotation: 147 + 'deg'
                        })
                        break;
                    case "5":
                        TweenMax.to(svgDay,0.4,{
                            rotation: 127 + 'deg'
                        });
                        break;
                    case "6":
                        TweenMax.to(svgDay,0.4,{
                            rotation: 100 + 'deg'
                        })
                        break;
                    case "7":
                        TweenMax.to(svgDay,0.4,{
                            rotation: 404 + 'deg'
                        })
                        break;
                    case "8":
                        TweenMax.to(svgDay,0.4,{
                            rotation: 5 + 'deg'
                        })
                        break;
                    default:
                        console.log("Désolé, nous n'avons plus de " + expr + ".");
                }
            }
        }
        let sliderDay = new Slider(document.querySelector('.slider-days'), false, true);
        sliderDay.init();

    },2100);

    let dataSecond = secondSlide.querySelector('.active-liste').textContent;

    tw.fromTo (btnSecond.parentNode.parentNode, 1, {opacity:1}, {opacity:0,display:'none'});
    TweenMax.delayedCall(2,() => {
        tw.fromTo (('.second_slide-template') , 1, {opacity:0}, {opacity:1,display:'block'})
    });


    // GRAPH DONUTS

    var dataset = [
        { label: 'Tous les jours', count: 46, day:1 },
        { label: 'Plusieurs fois par semaine', count: 4, day:2 },
        { label: 'Environ une fois par semaine', count: 5, day:3 },
        { label: 'Deux à trois fois par semaine', count: 7, day:4 },
        { label: 'Une fois par mois', count: 5, day:5 },
        { label: 'Une fois tous les deux à trois mois', count: 9, day:6 },
        { label: 'Moins souvent', count: 21, day:7 },
        { label: 'Jamais', count: 3, day:8 },
    ];
    var width = 320;
    var height = 320;
    var radius = Math.min(width, height) / 2;
    var donutWidth = 75;                            // NEW
    var color = d3.scaleOrdinal(d3.schemeCategory20b);
    var svg = d3.select('#donuts')
        .append('svg')
        .attr('width', width)
        .attr('height', height)
        .append('g')
        .attr('transform', 'translate(' + (width / 2) +
            ',' + (height / 2) + ')');
    var arc = d3.arc()
        .innerRadius(radius - donutWidth)             // UPDATED
        .outerRadius(radius);
    var pie = d3.pie()
        .value(function(d) { return d.count; })
        .sort(null);
    var path = svg.selectAll('path')
        .data(pie(dataset))
        .enter()
        .append('path')
        .attr('d', arc).style('stroke', '#101e3d')
        .attr('data-day', function(d) { return d.data.day; })
        .attr('fill', "white")
        .style("stroke-width", 8)  // colour the line
        .style("stroke-linejoin", "round")  // shape the line join

        var gradient = svg.append("linearGradient")
            .attr("id", "svgGradient")
            .attr("x2", "438.86")
            .attr("y1", "99.77")
            .attr("y2", "99.77")
            .attr('gradientUnits','userSpaceOnUse');

        gradient.append("stop")
            .attr('class', 'start')
            .attr("offset", "0%")
            .attr("stop-color", "#e5ea23")
            .attr("stop-opacity", 1);

        gradient.append("stop")
            .attr('class', 'end')
            .attr("offset", "1")
            .attr("stop-color", "#e08a36")
            .attr("stop-opacity", 1);



});

// PARTIE PAGE 4
var percantage = 0;
let radius = 155;
var CIRCUMFERENCE = 2 * Math.PI * radius;
Draggable.create(".block-drag", {
    type: "rotation",
    bounds:{minRotation:0, maxRotation:360},
    onDrag: function() {
        percantage = Math.round((this.rotation / 360) * 100);
        document.querySelector('.percantage').innerHTML = percantage;
        if(percantage === 100) {
            document.querySelector('.percantage').innerHTML ='+ ' + percantage ;
        }
        var progress = percantage / 100;
        var dashoffset = CIRCUMFERENCE * (1 - progress);
        tw.set('#teste', {
            strokeDashoffset: dashoffset,
        })
    }
});
var yourDraggable = Draggable.get(".block-drag");
let sliderFour = document.querySelector('.four-slide');
var btnFour = sliderFour.querySelector('.btn-valid');
const templateMoyenne = '<span class="moyenne">Moyenne en france par personne:</span>  ' +
    '<span>48</span>' +
    '<span class="switch-moyenne">' +
    '<a class="btn-moyenne btn-moy active" href="#">boites</a>' +
    '<a class="btn-moyenne btn-price" href="#">€</a>' +
    '</span>' ;
const templatePriceMoyenne = '<span class="moyenne">Moyenne en france par personne:</span>  ' +
    '<span>512 €</span>' +
    '<span class="switch-moyenne">' +
    '<a class="btn-moyenne btn-moy" href="#">boites</a>' +
    '<a class="btn-moyenne btn-price active" href="#">€</a>' +
    '</span>' ;

let percentageHtml =  document.querySelector('.percantage');
let button;
let dragTarget = document.querySelector('.block-drag--one').querySelector('.block-drag-target');

btnFour.addEventListener('click', (e) => {
    tw.fromTo (btnFour , 1, {opacity:1}, {opacity:0,display:'none'});
    e.preventDefault();
    var progress = 48 / 100;
    var dashoffset = CIRCUMFERENCE * (1 - progress);
    tw.set('#teste', {
        strokeDashoffset: dashoffset,
    });
    document.getElementById('teste').style.stroke = ('url(#gradientTwo');
    percentageHtml.style.color = '#df2828';
    percentageHtml.classList.add('text-center');
    percentageHtml.innerHTML = templateMoyenne;
    let div = document.querySelector('.four_slide-content');
    div.innerHTML += '<p class="four_content">Alors que la plupart des médicaments se périment avant d’avoir été consommés.</p>';
        tw.fromTo (('.four_content') , 1, {opacity:0}, {opacity:1,display:'block'})
    button = percentageHtml.querySelector('.btn-moy');
    TweenLite.to(".block-drag--two",0.5,{rotation: 170, display:'block'});
    yourDraggable.update();
    test2(button);
});

function test2(button) {
    document.querySelector('.btn-moy').addEventListener('click', function(e) {
        e.preventDefault();
        document.querySelector('.percantage').innerHTML = templateMoyenne;
        test2(button);
    });
    document.querySelector('.btn-price').addEventListener('click', (e) => {
        e.preventDefault();
        document.querySelector('.percantage').innerHTML = templatePriceMoyenne;
        test2(button);
    }, false);
}

// HOVER MEDIC
let body = window,
    panel = document.querySelector('.img-medic');

function initTilt() {
    TweenMax.set(panel, { transformStyle: "preserve-3d" });
    body.addEventListener('mousemove', (e) =>{
        var sxPos = e.pageX / body.innerWidth * 100 - 100;
        var syPos = e.pageY / body.innerHeight * 100 - 100;
        TweenMax.to(panel, 2, {
            rotationY: 0.03 * sxPos,
            rotationX: -0.03 * syPos,
            transformPerspective: 500,
            transformOrigin: "center center -400",
            ease: Expo.easeOut
        });
    });
};

// PAGE 3
let btnSlideThird = document.querySelectorAll('.third-slide .btn-choice');
for(let i = 0; btnSlideThird.length > i; i++) {
    btnSlideThird[i].addEventListener('click', (e) => {
        e.preventDefault();
        btnSlideThird[i].classList.toggle('btn-last')
    })
}

initTilt();

document.querySelector('.btn-list').addEventListener('click', (e) => {
    e.preventDefault();
    tw.fromTo (('.thank') , 1, {opacity:1}, {opacity:0,display:'none'});
    TweenMax.delayedCall(1,() => {
        tw.fromTo (('.slide-last') , 1, {opacity:0}, {opacity:1,display:'block'})
    });
})
document.querySelector('.template-result--slide-last .btn-next-two').addEventListener('click', (e) => {
    e.preventDefault();
    tw.fromTo (('.template-result--slide-last') , 1, {opacity:1}, {opacity:0,display:'none'});
    TweenMax.delayedCall(1,() => {
        tw.fromTo (('.source') , 1, {opacity:0}, {opacity:1,display:'block'})
    });

})